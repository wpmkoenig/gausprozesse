'''#############################################################################'''
'''Autor: Luca König'''
'''Datum: September 2020'''
'''Programm: Darstellung verschiedener Kernelfunktion                           '''
'''#############################################################################'''
'''############################ START: Imports #################################'''
#Visualisation
import matplotlib.pyplot as plt
#Maths and Calculation
import math
import numpy as np
from numpy.linalg import inv
from scipy.stats import norm

x0 = 0

def SquaredExponential(x,y,sigma,l):
    return sigma*np.exp(-(np.subtract.outer(x,y)**2)/(2*l**2))

def Kovarianz(sigma,l):
    covariance = []
    for x in np.arange(-5,5,0.01):
        covariance = np.append(covariance, SquaredExponential(x0, x, sigma, l))
    return covariance

plt.plot(np.arange(-5,5,0.01), Kovarianz(1,1), 'b-', markersize=1, label=r"$\sigma = 1, \l = 1$")
# plt.plot(np.arange(0,5,0.01), Kovarianz(2,1), 'r-', markersize=1, label=r"$\sigma = 2, \l = 1$")
# plt.plot(np.arange(0,5,0.01), Kovarianz(0.5,1), 'g-', markersize=1, label=r"$\sigma = 0.5, \l = 1$")
plt.xlabel("x-x'")
plt.ylabel("k(x,x')")
# plt.title('Kovarianz mit Squared-Exponential-Kernelfunktion')
plt.legend()
plt.show()

plt.plot(np.arange(-5,5,0.01), Kovarianz(2,1), 'b-', markersize=1, label=r"$\sigma = 2, \l = 1$")
plt.plot(np.arange(-5,5,0.01), Kovarianz(1,1), 'r-', markersize=1, label=r"$\sigma = 1, \l = 1$")
plt.plot(np.arange(-5,5,0.01), Kovarianz(0.5,1), 'g-', markersize=1, label=r"$\sigma = 0.5, \l = 1$")
plt.xlabel("x-x'")
plt.ylabel("k(x,x')")
# plt.title('Wirkung von $\l$ auf die Squared-Exponential-Kernelfunktion')
plt.legend()
plt.show()
