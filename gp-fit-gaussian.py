'''#############################################################################'''
'''Autor: Luca König'''
'''Datum: September 2020'''
'''Programm: Curve-Fitting von Parametern und Gauss Prozess über Messreihe      '''
'''#############################################################################'''
'''############################ START: Imports #################################'''

import sys
import matplotlib.pyplot as plt
import math
import numpy as np
from numpy.linalg import inv
import scipy.io as sio
from scipy.optimize import curve_fit
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, ConstantKernel as C, RationalQuadratic as RQ, WhiteKernel as WHT, ExpSineSquared as EXP, Matern as MAT
from sklearn import preprocessing
# np.set_printoptions(threshold=sys.maxsize)

mat_data = sio.loadmat('C:/Users/Luca/Documents/03_Studium/Hilfskraft-Job/Code/exoskelett/DP_Reconstruct/Matlab/Data2_noisy.mat') #Einlesen der .mat-Datei
data_mess = mat_data['Save_Data2_noisy']    #Filtern des Datenarrays

'''############################ END: Imports ###################################'''
'''#############################################################################'''
'''############################ START: Parameter ###############################'''

#gravitational acceleration
g = 9.8

#trainig and validation
training = 1000

#measurement data
x1 = data_mess[:training,0]  #Theta1
x2 = data_mess[:training,1]  #Theta2
T1 = data_mess[:training,2]  #M1
T2 = data_mess[:training,3]  #M2
combination = np.concatenate((x1,x2))
print("len(x1):", len(x1))
print("combination.shape:", combination.shape)

#for sampling time 15s
T = 0.010 #incremental time step
recordtime = training*T #in s
timesteps = training #amount of time steps while recordtime
posteriorsteps = 50
xdata = np.linspace(0,recordtime*2,timesteps*2)
print("len(xdata):", len(xdata))

'''############################ END: Parameter #################################'''
'''#############################################################################'''
'''############################ START: Functions ###############################'''

def fitting(xdata, m1, m2, l1, l2):
    global timesteps
    #empty inital arrays
    # m1 = m1*10
    # m2 = m2*10
    th1 = np.array([0,0])
    th2 = np.array([0,0])
    for k in range(0,timesteps):
        #Simplificated parameters
        a1 = (m1+m2)*l1**2
        a2 = m2*l1*l2
        a3 = (m1+m2)*l1
        a4 = m2*l2**2
        a5 = m2*l2

        #Dynamics Simplification
        A = a1*a4-(a2*np.cos(th1[k]-th2[k]))**2
        s1 = -a2*((th2[k+1]-th2[k])/T)**2*np.sin(th1[k]-th2[k])-g*a3*np.sin(th1[k])+T1[k]
        s2 = a2*((th1[k+1]-th1[k])/T)**2*np.sin(th1[k]-th2[k])-g*a5*np.sin(th2[k])+T2[k]

        #Pendulum Dynamic
        th1 = np.append(th1, (2*th1[k+1]-th1[k]+T**2*(a4*s1-s2*a2*np.cos(th1[k]-th2[k]))/A))
        th2 = np.append(th2, (2*th2[k+1]-th2[k]+T**2*(-s1*a2*np.cos(th1[k]-th2[k])+a1*s2)/A))
    combination_fit = np.concatenate((th1[:-2],th2[:-2]))
    return combination_fit

'''############################ END: Functions #################################'''
'''#############################################################################'''
'''############################ START: Curve Fit ###############################'''

reference = fitting(xdata,8,5,0.5,0.4)
print("len(th1):", len(reference))
print("th1.shape:", reference.shape)
popt, pcov = curve_fit(fitting, xdata, combination, method='trf', bounds=([1, 1, 0.2, 0.1], [20, 15, 0.8, 0.5]), xtol=0.01) #bounds=([3, 2, 0.2, 0.1], [10, 8, 0.8, 0.5])
print(popt)
m1, m2, l1, l2 = popt
combination_fit = fitting(xdata, m1, m2, l1, l2)

#split combined arrays
th1_fit, th2_fit = np.array_split(combination_fit, 2)
th1_ref, th2_ref = np.array_split(reference, 2)
th1_mess, th2_mess = np.array_split(combination, 2)
xdata, xdata2  = np.array_split(xdata, 2)

'''############################ END: Curve Fit #################################'''
'''#############################################################################'''
'''############################ START: Gaussian Process ########################'''

#Anzahl der Zeitschritte für Regression
dataframes = np.arange(timesteps)
X = np.atleast_2d([dataframes]).T

#Anzahl der Zeitschritte für Vorhersage
dataframes = np.arange(timesteps+posteriorsteps)
X_prior = np.atleast_2d([dataframes]).T

#Vorhersage Zeitschritte
x = np.atleast_2d(np.linspace(1,timesteps+posteriorsteps,(timesteps+posteriorsteps)*10)).T

#Gauss Prozess Modell instanziieren
kernel = MAT(length_scale=10, nu=5/2) + WHT(noise_level=1)
gp = GaussianProcessRegressor(kernel=kernel, n_restarts_optimizer=3)
gp.fit(X,th1_mess)
y_pred_1, sigma_1 = gp.predict(x, return_std=True)
print("Vorhersage Interpolationsschritte:", len(y_pred_1))

'''############################ END: Gaussian Process ##########################'''
'''#############################################################################'''
'''############################ START: Visualisation ###########################'''

print("Laenge X:", len(X))
print("Laenge X_prior:", len(X_prior))
print("Laenge xdata:", len(xdata))

plt.subplot(311)
th1_eq = th1_mess-th1_fit
plt.plot(xdata, th1_mess, 'ro', markersize=1, label=u'Th1 gemessen')
plt.plot(xdata, th1_fit, 'k-', markersize=1, label=u'Th1 gefittet')
plt.xlabel('Timesteps in s')
plt.ylabel('Angle in rad')
plt.title('Theta 1 gleichwertfrei, gemessen und gefittet')
plt.legend()
plt.subplot(312)
plt.plot(X, th1_mess, 'ro', markersize=2, alpha=1, label=u'Observation')
plt.plot(X_prior, data_mess[:(training+posteriorsteps),0], 'bo', markersize=2, alpha=0.2, label=u'Observation')
plt.plot(x[:,0], y_pred_1, 'b-', linewidth=1, label=u'Prediction')
plt.fill_between(x[:,0], y_pred_1 - 1.96*sigma_1,y_pred_1 + 1.96*sigma_1, alpha=0.2, color='k', label=u'95 % confidence interval')
plt.xlabel('Timesteps')
plt.legend()
plt.subplot(313)
plt.plot(X, th1_mess, 'ro', markersize=2, alpha=1, label=u'Observation')
plt.plot(X, th1_fit, 'k-', markersize=1, label=u'Th1 gefittet')
plt.plot(X_prior, data_mess[:(training+posteriorsteps),0], 'bo', markersize=2, alpha=0.2, label=u'Observation')
plt.plot(x[:,0], y_pred_1, 'b-', linewidth=1, label=u'Prediction')
plt.fill_between(x[:,0], y_pred_1 - 1.96*sigma_1,y_pred_1 + 1.96*sigma_1, alpha=0.2, color='k', label=u'95 % confidence interval')
plt.xlabel('Timesteps')
plt.show()
