'''#############################################################################
Autor:
    Luca König
Datum:
    August 2020
Beschreibung:
    Darstellung aller Werte im Zeitverlauf beider Winkel

################################################################################'''
'''############################ START: Imports #################################'''

import numpy as np
import scipy.io as sio
from matplotlib import pyplot as plt
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

fig = plt.figure()
ax = fig.gca(projection='3d')

#Einlesen der .mat-Datei
mat_ang_hip = sio.loadmat('C:/Users/Luca/Documents/03_Studium/Hilfskraft-Job/Code/exoskelett/DP_Reconstruct/Python/ascent_hip_angles.mat')
mat_ang_knee = sio.loadmat('C:/Users/Luca/Documents/03_Studium/Hilfskraft-Job/Code/exoskelett/DP_Reconstruct/Python/ascent_knee_angles.mat')
#Filtern des Datenarrays
data_ang_hip = mat_ang_hip['hip']
data_ang_knee = mat_ang_knee['knee']

'''############################ END: Imports ###################################'''
'''#############################################################################'''
'''############################ START: Data Troubleshooting ####################'''

'''############################ END: Data Troubleshooting ######################'''
'''#############################################################################'''
'''############################ START: Parameter ###############################'''

#Schneide Spalten vorn und hinten am Array ab
source_data_slice_start = 0
source_data_slice_end = 300
data_ang_hip = data_ang_hip[:,source_data_slice_start:source_data_slice_end]
data_ang_knee = data_ang_knee[:,source_data_slice_start:source_data_slice_end]

print(data_ang_hip)

# data_ang_hip = np.reshape(data_ang_hip,(1, -1))
# data_ang_knee = np.reshape(data_ang_knee,(1, -1))

#Wählt einen Datensatz an
multiplicant = 0
datarange = source_data_slice_end-source_data_slice_start
startslice = datarange*multiplicant
width = 1
endslice = startslice+datarange*width

'''############################ END: Parameter  ##################'''
'''#############################################################################'''
'''############################ START: Slicing Single Range ####################'''

x1 = (data_ang_hip[:,startslice:endslice])*180/np.pi  #Theta1
x2 = (((data_ang_knee[:,startslice:endslice])*180/np.pi)+x1) #Theta2
print("len x2: ",len(x2))

#Notwendig für Anzeige aller Verläufe
x1 = x1.T
x2 = x2.T
X, Y = np.meshgrid(x1, x2)

plt.xlabel('Row Dataset')
plt.ylabel('Value')
length_axis = len(x1)
# length_axis = len(x2)
offset = 0
Z = np.add(*np.indices((length_axis, length_axis)))

print(Z)
# plt.plot(xaxis, x1, 'o', markersize='1', label='x1')
# plt.plot(xaxis, x2, 'o', markersize='1', label='x1')
# plt.legend(loc='upper left')
#
# surf = ax.plot_surface(X, Y, Z, cmap=cm.coolwarm, linewidth=0, antialiased=False)
# fig.colorbar(surf, shrink=0.5, aspect=5)
# plt.show()
