'''#############################################################################'''
'''Autor: Luca König'''
'''Datum: September 2020'''
'''Programm: Darstellung der synthetischen Signale und Messdaten                '''
'''#############################################################################'''
'''############################ START: Imports #################################'''

import numpy as np
import scipy.io as sio
from matplotlib import pyplot as plt
from sklearn import preprocessing

#Einlesen der .mat-Datei
# synth_data = sio.loadmat('C:/Users/Luca/Documents/03_Studium/6.Semester/Bachelorarbeit/Code/Messdaten/synthetical_data_parallel_noisefree.mat')
synth_data = sio.loadmat('C:/Users/Luca/Documents/03_Studium/6.Semester/Bachelorarbeit/Code/Messdaten/synthetical_data_parallel_noise5per.mat')
# synth_data = sio.loadmat('C:/Users/Luca/Documents/03_Studium/6.Semester/Bachelorarbeit/Code/Messdaten/synthetical_data_parallel_noise10per.mat')
# synth_data = sio.loadmat('C:/Users/Luca/Documents/03_Studium/6.Semester/Bachelorarbeit/Code/Messdaten/synthetical_data_parallel_noise20per.mat')

data_mat = synth_data['Synth_Data_Parallel']

'''############################ END: Imports ###################################'''
'''#############################################################################'''
'''############################ START: Data Troubleshooting ####################'''

'''############################ END: Data Troubleshooting ######################'''
'''#############################################################################'''
'''############################ START: Parameter ###############################'''

#Schneide Spalten vorn und hinten am Array ab
source_data_slice_start = 200
source_data_slice_end = 300
fromstart = 56
howmuch = 59
data_ang_hip = data_mat[source_data_slice_start:source_data_slice_end, 0, fromstart:howmuch]
data_ang_knee = data_mat[source_data_slice_start:source_data_slice_end, 1, fromstart:howmuch]
data_torq_hip = data_mat[source_data_slice_start:source_data_slice_end, 2, fromstart:howmuch]
data_torq_knee = data_mat[source_data_slice_start:source_data_slice_end, 3, fromstart:howmuch]

print(np.shape(data_ang_hip))

# data_ang_hip = np.reshape(data_ang_hip,(1, -1))
# data_ang_knee = np.reshape(data_ang_knee,(1, -1))

#Wählt einen Datensatz an
multiplicant = 0
datarange = source_data_slice_end-source_data_slice_start
startslice = datarange*multiplicant
width = 1
endslice = startslice+datarange*width

'''############################ END: Parameter  ################################'''
'''#############################################################################'''
'''############################ START: Slicing Single Range ####################'''

x1 = np.degrees(data_ang_hip) #Theta1
x2 = np.degrees(data_ang_knee) #Theta2
x3 = data_torq_hip
x4 = data_torq_knee

# x1 = x1_scaled = preprocessing.scale(x1)
# x2 = x2_scaled = preprocessing.scale(x2)

print("len x2: ",len(x2))

plt.figure(num=None, dpi=150, figsize=(9, 8), facecolor='w', edgecolor='k')
plt.subplot(211)
plt.xlabel('t in ms')
plt.ylabel(r'$\theta_1$ in $\degree$ ')
length_axis = len(x1)
offset = 0
xaxis = list(range(offset,length_axis*10,10))
plt.plot(xaxis, x1, 'o', markersize='1', label='x1')

plt.subplot(212)
plt.xlabel('t in ms')
plt.ylabel(r'$T_1$ in $N$ ')
length_axis = len(x3)
offset = 0
xaxis = list(range(offset,length_axis*10,10))
plt.plot(xaxis, x3, 'o', markersize='1', label='x3')
plt.show()
