'''#############################################################################'''
'''Autor: Luca König'''
'''Datum: September 2020'''
'''Programm: Online Gauss Process                                               '''
'''#############################################################################'''
'''############################ START: Imports #################################'''

#Visualisation
import matplotlib.pyplot as plt
#Maths and Calculation
import math
import numpy as np
from numpy.linalg import inv
#Input Matlab File
import scipy.io as sio
#Gaussian Process
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, ConstantKernel as C, RationalQuadratic as RQ, WhiteKernel as WHT, ExpSineSquared as EXP, Matern as MAT
from sklearn import preprocessing
#time
import time

#Load Dataset from .mat-File
mat_data = sio.loadmat('C:/Users/Luca/Documents/03_Studium/Hilfskraft-Job/Code/exoskelett/DP_Reconstruct/Matlab/Data2_noisy.mat') #Einlesen der .mat-Datei
data_mess = mat_data['Save_Data2_noisy']    #Filtern des Datenarrays

'''############################ END: Imports ###################################'''
'''#############################################################################'''
'''############################ START: Parameter ###############################'''

#set of training data
training = 1000

#measurement data
x1 = data_mess[:training,0]  #Theta1
x2 = data_mess[:training,1]  #Theta2
T1 = data_mess[:training,2]  #M1
T2 = data_mess[:training,3]  #M2

xdata = []
ydata = []

#time
time = 0.01

'''############################ END: Parameter #################################'''
'''#############################################################################'''
'''############################ START: Gaussian Process ########################'''


'''############################ END: Gaussian Process ##########################'''
'''#############################################################################'''
'''############################ START: Debugging ###############################'''

print("len(x1):", len(x1))

'''############################ END: Debugging #################################'''
'''#############################################################################'''
'''############################ START: Visualisation ###########################'''

plt.show()
#current axes instance
axes = plt.gca()
axes.set_xlim(0,1000)
axes.set_ylim(-0.1, +0.1)
line, = axes.plot(xdata, ydata, 'ro', markersize=1)

for i in range(len(x1)):
    xdata.append(i)
    ydata.append(x1[i])
    line.set_xdata(xdata)
    line.set_ydata(ydata)
    plt.draw()
    plt.pause(time)
plt.show()
