'''#############################################################################'''
'''Autor: Luca König'''
'''Datum: September 2020'''
'''Programm: Testfile for snippets                                              '''
'''#############################################################################'''
'''############################ START: Imports #################################'''

import numpy as np
import time
import matplotlib.pyplot as plt

x1_storage = np.random.randint(10,size=(3,10))
i = 0
x1_pipe = np.empty([3,0])

hl, = plt.plot([], [])

def getShape(a):
    if len(a.shape) > 1:
        rows = a.shape[0]
        cols = a.shape[1]
    else:
        rows = a.shape[0]
        cols = 0
    return (rows, cols)

def pipeline(x1_storage):
    global x1_pipe
    (nRows,nCols) = getShape(x1_storage[:,:i])
    x1_append = np.array([x1_storage[:,nCols-1]]).T
    x1_pipe = np.hstack((x1_pipe,x1_append))
    return x1_pipe

def update_plot(hl, xdata, new_data):
    hl.set_xdata(np.append(hl.get_xdata(), new_data))
    hl.set_ydata(np.append(hl.get_ydata(), xdata))
    plt.draw()

while i<5:
    x1_pipe = pipeline(x1_storage)
    print(x1_pipe)
    (nRows, nCols) = np.shape(x1_pipe)
    update_plot(hl, nRows, x1_pipe[:,i])
    #for pipeline simulation
    time.sleep(1)
    i = i+1
