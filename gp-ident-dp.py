'''#############################################################################'''
'''Autor: Luca König'''
'''Datum: September 2020'''
'''Programm: Modellidentifikation mit Gauss Prozess und DP-Identifikation für
             einen Datenframe                                                   '''
'''#############################################################################'''
'''############################ START: Imports #################################'''
#Visualisation
import matplotlib.pyplot as plt
#Maths and Calculation
import math
import numpy as np
from numpy.linalg import inv
from scipy.stats import norm
#Input Matlab File
import scipy.io as sio
#Curve Fitting Library
from scipy.optimize import curve_fit
#Gaussian Process
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, ConstantKernel as C, RationalQuadratic as RQ, WhiteKernel as WHT, ExpSineSquared as EXP, Matern as MAT
from sklearn import preprocessing
#time
import time
#import MODUL for Double Pendulum Dynamics
import module_dp as dp

#Dataset of Measurement Values
# mat_ang_hip = sio.loadmat('C:/Users/Luca/Documents/03_Studium/6.Semester/Bachelorarbeit/Code/Messdaten/ascent_hip_angles.mat') #Einlesen der .mat-Datei
# mat_ang_knee = sio.loadmat('C:/Users/Luca/Documents/03_Studium/6.Semester/Bachelorarbeit/Code/Messdaten/ascent_knee_angles.mat') #Einlesen der .mat-Datei
# data_ang_hip = mat_ang_hip['hip']
# data_ang_knee = mat_ang_knee['knee']
# data_ang_hip = data_ang_hip.T
# data_ang_knee = data_ang_knee.T

#Einlesen der .mat-Datei
# synth_data = sio.loadmat('C:/Users/Luca/Documents/03_Studium/6.Semester/Bachelorarbeit/Code/Messdaten/synthetical_data_parallel_noisefree.mat')
synth_data = sio.loadmat('C:/Users/Luca/Documents/03_Studium/6.Semester/Bachelorarbeit/Code/Messdaten/synthetical_data_parallel_noise5per.mat')
# synth_data = sio.loadmat('C:/Users/Luca/Documents/03_Studium/6.Semester/Bachelorarbeit/Code/Messdaten/synthetical_data_parallel_noise10per.mat')
# synth_data = sio.loadmat('C:/Users/Luca/Documents/03_Studium/6.Semester/Bachelorarbeit/Code/Messdaten/synthetical_data_parallel_noise20per.mat')
data_mat = synth_data['Synth_Data_Parallel']

'''############################ END: Imports ###################################'''
'''#############################################################################'''
'''############################ START: Parameter ###############################'''

### for real measurement data
#choice of frames
sets = 6
#steps of trainingdata
steps = 1
offset = 202 * steps
#width of trainingset per frame
training = 200*steps
#Theta 1 and Theta 2 values
x1 = data_mat[offset:(training+offset):steps, 0, sets]  #Theta1
x1 = x1.reshape(-1)
x2 = data_mat[offset:(training+offset):steps, 1, sets]  #Theta2
x2 = x2.reshape(-1)
x3 = data_mat[offset:(training+offset):steps, 2, sets]  #Theta2
x3 = x3.reshape(-1)
x4 = data_mat[offset:(training+offset):steps, 3, sets]  #Theta2
x4 = x4.reshape(-1)
#for curve fitting
combination = np.concatenate((x1,x2))

### for dp identification
#gravitational acceleration
g = 9.8

### scaling parameters
#time distance between samples
T = 0.01
#Recordtime
recordtime = training*T
#Iterations for curve fitting
its = int(training/steps)
#scaling axis for combinated data
xdata = np.linspace(0,recordtime*2,its*2)

'''############################ END: Parameter #################################'''
'''#############################################################################'''
'''############################ START: Functions ###############################'''

def dynamic(xdata, m1, m2, l1, l2):
    #empty inital arrays
    # m1 = m1*10
    # m2 = m2*10
    th1 = data_mat[(offset-2):offset:steps, 0, sets]
    th2 = data_mat[(offset-2):offset:steps, 1, sets]
    for k in range(0,its):
        #Simplificated parameters
        a1 = (m1+m2)*l1**2
        a2 = m2*l1*l2
        a3 = (m1+m2)*l1
        a4 = m2*l2**2
        a5 = m2*l2

        #Dynamics Simplification
        A = a1*a4-(a2*np.cos(th1[k]-th2[k]))**2
        s1 = -a2*((th2[k+1]-th2[k])/T)**2*np.sin(th1[k]-th2[k])-g*a3*np.sin(th1[k])+x3[k]
        s2 = a2*((th1[k+1]-th1[k])/T)**2*np.sin(th1[k]-th2[k])-g*a5*np.sin(th2[k])+x4[k]

        #Pendulum Dynamic
        th1 = np.append(th1, (2*th1[k+1]-th1[k]+T**2*(a4*s1-s2*a2*np.cos(th1[k]-th2[k]))/A))
        th2 = np.append(th2, (2*th2[k+1]-th2[k]+T**2*(-s1*a2*np.cos(th1[k]-th2[k])+a1*s2)/A))
    combination_fit = np.concatenate((th1[:-2],th2[:-2]))
    return combination_fit

'''############################ END: Functions #################################'''
'''#############################################################################'''
'''############################ START: Gaussian Distribution ###################'''

popt, pcov = curve_fit(dynamic, xdata, combination, method='trf', bounds=([6,6,0.1,0.1],[7,7,0.2,0.2]), xtol=0.001) #[1, 1, 0.1, 0.1], [10, 10, 1, 1]), xtol=0.001)
print(popt)
m1, m2, l1, l2 = popt
combination_fit = dynamic(xdata, m1, m2, l1, l2)

#split combined arrays
th1_fit, th2_fit = np.degrees(np.array_split(combination_fit, 2))
th1_mess, th2_mess = np.degrees(np.array_split(combination, 2))
th1_dif = th1_mess - th1_fit
th2_dif = th2_mess - th2_fit
xdata, xdata2  = np.array_split(xdata, 2)

#Anzahl der Zeitschritte für Regression
dataframes = np.arange(its)
X = np.atleast_2d([dataframes]).T
#Anzahl der Zeitschritte für Vorhersage
dataframes = np.arange(its)
X_prior = np.atleast_2d([dataframes]).T
#Vorhersage Zeitschritte
x = np.atleast_2d(np.linspace(1,its,(its)*10)).T

#Gauss Prozess Modell instanziieren
# kernel = RBF(length_scale=10)+WHT(noise_level=1)
kernel = MAT(length_scale=1, nu=3/2) + WHT(noise_level=1)
gp = GaussianProcessRegressor(kernel=kernel, n_restarts_optimizer=10)
gp.fit(X,th1_dif)
y_pred_1, sigma_1 = gp.predict(x, return_std=True)

#add gp to fit and scale
th1_fitgp_mu = th1_fit + y_pred_1[::10]
#scale down for visualisation
th1_fitgp_sigma = sigma_1[::10]
#residual noise after fitting and gaussian process
th1_fitgp_noise = th1_mess - th1_fitgp_mu
th1_fit_noise = th1_mess - th1_fit

'''############################ END: Program Run ###############################'''
'''#############################################################################'''
'''############################ START: Visualisation ###########################'''
plt.figure(num=None, dpi=150, facecolor='w', edgecolor='k')
plt.plot(xdata, th1_fit, 'b-', label=r'$\theta_{1,genähert}$')
plt.plot(xdata, th1_mess, 'ro', markersize=1, label=r'$\theta_{1,gemessen}$')
plt.xlabel('T in s')
plt.ylabel(r'$\theta_1$ in $\circ$')
# plt.title('Doppelpendel')
plt.legend(loc="lower right")
plt.show()

plt.figure(num=None, dpi=150, facecolor='w', edgecolor='k')
plt.plot(xdata, th1_dif, 'ro', markersize=2, label=r'$\theta_{1,gemessen - \theta_1,genähert}$')
plt.plot(x*0.01*steps, y_pred_1, 'k-', markersize=1, label=r'$\theta_{1,GP}$')
plt.fill_between(x[:,0]*0.01*steps, y_pred_1 - 1.96*sigma_1,y_pred_1 + 1.96*sigma_1, alpha=0.2, color='k', label=u'95 % Vertrauensintervall')
plt.xlabel('T in s')
plt.ylabel(r'$\theta$ in $\circ$')
# plt.title('Gaußprozess')
plt.legend(loc="lower right")
plt.show()

plt.figure(num=None, dpi=150, facecolor='w', edgecolor='k')
plt.plot(xdata, th1_fitgp_mu, 'g-', markersize=1, label=r'$\theta_{1,DP+GP}$')
plt.plot(xdata, th1_fit, 'b-', markersize=1, label=r'$\theta_{1,genähert}$')
plt.plot(xdata, th1_mess, 'ro', markersize=2, label=r'$\theta_{1,gemessen}$')
plt.fill_between(xdata, th1_fitgp_mu - 1.96*th1_fitgp_sigma,th1_fitgp_mu + 1.96*th1_fitgp_sigma, alpha=0.2, color='k', label=u'95 % Vertrauensintervall')
plt.xlabel('T in s')
plt.ylabel(r'$\theta_1$ in $\circ$')
# plt.title('DP, DP+GP und Messwerte')
plt.legend(loc="lower right")
plt.show()

plt.figure(num=None, dpi=150, facecolor='w', edgecolor='k')
plt.plot(xdata, th1_fitgp_noise, 'bo', markersize=2, label=r'$\theta_{1,Rest,DP+GP}$')
plt.plot(xdata, th1_fit_noise, 'ro', markersize=2, label=r'$\theta_{1,Rest,DP}$')
plt.xlabel('T in s')
plt.ylabel(r'$\theta_1$ in $\circ$')
# plt.title('Restrauschen von DP und GP')
plt.legend(loc="lower right")
plt.show()

print("Maximum Restrauschen Modell:", max(np.abs(th1_fit_noise)).round(2))
print("Maximum Restrauschen Modell + GP:", max(np.abs(th1_fitgp_noise)).round(2))
print("Mittelwert Restrauschen Modell:", np.mean(np.abs(th1_fit_noise)).round(2))
print("Mittelwert Restrauschen Modell + GP:", np.mean(np.abs(th1_fitgp_noise)).round(2))

print(max(th1_mess))

'''############################ END: Visualisation #############################'''
'''#############################################################################'''
