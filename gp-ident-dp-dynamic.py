'''#############################################################################'''
'''Autor: Luca König'''
'''Datum: August 2020'''
'''Programm: Modellidentifikation mit Gaussprozess und DP-Identifikation
             für multiple Datenframes                                           '''
'''#############################################################################'''
'''############################ START: Imports #################################'''

#Saving Configurations
td = 100
file = "V1-R5-DPM1-GPK0-TD{}-SW1-MW1-appendix".format(td)
rauschen = "nein"
dp_anzahl = "einfach"

#Visualisation
import matplotlib.pyplot as plt
#Maths and Calculation
import math
import numpy as np
np.set_printoptions(suppress=True)
from numpy.linalg import inv
from scipy.stats import norm
#Import Matlab Files
import scipy.io as sio
#Curve Fitting Library
from scipy.optimize import curve_fit
#Gaussian Process
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import WhiteKernel as WHT, Matern as MAT
from sklearn import preprocessing
#time
import time

# Read .mat-file
# synth_data = sio.loadmat('C:/Users/Luca/Documents/03_Studium/6.Semester
#    /Bachelorarbeit/Code/Messdaten/synthetical_data_parallel_noisefree.mat')
synth_data = sio.loadmat('C:/Users/Luca/Documents/03_Studium/6.Semester/Bachelorarbeit/Code/Messdaten/synthetical_data_parallel_noise5per.mat')
# synth_data = sio.loadmat('C:/Users/Luca/Documents/03_Studium/6.Semester
    # /Bachelorarbeit/Code/Messdaten/synthetical_data_parallel_noise10per.mat')
# synth_data = sio.loadmat('C:/Users/Luca/Documents/03_Studium/6.Semester
    # /Bachelorarbeit/Code/Messdaten/synthetical_data_parallel_noise20per.mat')
# synth_data = sio.loadmat('C:/Users/Luca/Documents/03_Studium/6.Semester
    # /Bachelorarbeit/Code/Messdaten/synthetical_data_parallel_noise30per.mat')
data_mat = synth_data['Synth_Data_Parallel']

'''############################ END: Imports ###################################'''
'''#############################################################################'''
'''############################ START: Parameter ###############################'''

### for real measurement data
#choice of frames
sets = 100
#steps of trainingdata
steps = 1
offset = 2*steps
#width of trainingset per frame
training = td*steps
#theta 1 and theta 2 values
x1_storage = data_mat[offset:(training+offset):steps, 0, :sets]  #Theta1
x2_storage = data_mat[offset:(training+offset):steps, 1, :sets]  #Theta2
x3_storage = data_mat[offset:(training+offset):steps, 2, :sets]  #T1
x4_storage = data_mat[offset:(training+offset):steps, 3, :sets]  #T2
x1_pipe = np.empty([int(training/steps),0])
x2_pipe = np.empty([int(training/steps),0])
x3_pipe = np.empty([int(training/steps),0])
x4_pipe = np.empty([int(training/steps),0])
th1_dif = np.empty([int(training/steps),0])
th2_dif = np.empty([int(training/steps),0])

### for dp identification
#gravitational acceleration
g = 9.8

### scaling parameters
#time distance between samples
T = 0.01*steps
#recordtime
recordtime = training*T/steps
#iterations for curve fitting
its = int(training/steps)

### For dynamic updating dataset
#storage for cleaned noise after Gaussian Process
storage = []
#iterative variable for pipeline
i = 0
#storage for dynamic plot
h1, = plt.plot([],[])

#storage for analysis
mse_time_dp = []
mse_time_gp = []
mse_noise_dp = []
mse_noise_gp = []
rmse_noise_dp = []
rmse_noise_gp = []

#mean over how much GPs
meanover = 1

##plot configurations
plt.ion()
fig, [[ax1, ax2], [ax3, ax4]] = plt.subplots(nrows=2,ncols=2)
ax1.set_xlabel('Timesteps in s')
ax1.set_ylabel('Angle in rad')
ax1.set_title('Theta 1 Gefittet')
ax2.set_xlabel('Timesteps in s')
ax2.set_ylabel('Angle in rad')
ax2.set_title('Theta 1 GP über Messwerte - Fit')
ax3.set_xlabel('Timesteps in s')
ax3.set_ylabel('Angle in rad')
ax3.set_title('Theta 1 Fit + GP')
ax4.set_xlabel('Timesteps in s')
ax4.set_ylabel('Angle in rad')
ax4.set_title('Theta 1 Residual Noise after Fit + GP')

'''############################ END: Parameter #################################'''
'''#############################################################################'''
'''############################ START: Functions ###############################'''

def dynamic(xdata, m1, m2, l1, l2):
    #empty inital arrays
    th1 = data_mat[0:offset:steps, 0, i]
    th2 = data_mat[0:offset:steps, 1, i]
    for k in range(0,its):
        #simplificated parameters
        a1 = (m1+m2)*l1**2
        a2 = m2*l1*l2
        a3 = (m1+m2)*l1
        a4 = m2*l2**2
        a5 = m2*l2

        #dynamics Simplification
        A = a1*a4-(a2*np.cos(th1[k]-th2[k]))**2
        s1 = -a2*((th2[k+1]-th2[k])/T)**2*np.sin(th1[k]-th2[k])-g*a3*np.sin(th1[k])+x3[k]
        s2 = a2*((th1[k+1]-th1[k])/T)**2*np.sin(th1[k]-th2[k])-g*a5*np.sin(th2[k])+x4[k]

        #pendulum Dynamic
        th1 = np.append(th1, (2*th1[k+1]-th1[k]+T**2*(a4*s1-s2*a2*np.cos(th1[k]-th2[k]))/A))
        th2 = np.append(th2, (2*th2[k+1]-th2[k]+T**2*(-s1*a2*np.cos(th1[k]-th2[k])+a1*s2)/A))
    combination_fit = np.concatenate((th1[:-2],th2[:-2]))
    return combination_fit

def getShape(a):
    if len(a.shape) > 1:
        rows = a.shape[0]
        cols = a.shape[1]
    else:
        rows = a.shape[0]
        cols = 0
    return (rows, cols)

def pipeline(x1_storage, x2_storage, x3_storage, x4_storage):
    global x1_pipe, x2_pipe, x3_pipe, x4_pipe
    (nRows,nCols) = getShape(x1_storage[:,:i])
    x1_append = np.array([x1_storage[:,nCols]]).T
    x1_pipe = np.hstack((x1_pipe,x1_append))
    (nRows,nCols) = getShape(x2_storage[:,:i])
    x2_append = np.array([x2_storage[:,nCols]]).T
    x2_pipe = np.hstack((x2_pipe,x2_append))
    (nRows,nCols) = getShape(x3_storage[:,:i])
    x3_append = np.array([x3_storage[:,nCols]]).T
    x3_pipe = np.hstack((x3_pipe,x3_append))
    (nRows,nCols) = getShape(x4_storage[:,:i])
    x4_append = np.array([x4_storage[:,nCols]]).T
    x4_pipe = np.hstack((x4_pipe,x4_append))
    return x1_pipe, x2_pipe, x3_pipe, x4_pipe

'''############################ END: Functions #################################'''
'''#############################################################################'''
'''############################ START: Pipeline Simulation #####################'''

while i<sets:
    #scaling axis for combinated data
    xdata = np.linspace(0,recordtime*2,its*2)

    x1, x2, x3, x4 = pipeline(x1_storage, x2_storage, x3_storage, x4_storage)
    x1 = x1[:, -1:]
    x2 = x2[:, -1:]
    x3 = x3[:, -1:]
    x4 = x4[:, -1:]

    '''############################ END: Pipeline Simulation #######################'''
    '''#############################################################################'''
    '''############################ START: Gaussian Distribution ###################'''

    x1 = x1.reshape(-1)
    x2 = x2.reshape(-1)
    x3 = x3.reshape(-1)
    x4 = x4.reshape(-1)

    #for curve fitting
    combination = np.concatenate((x1,x2))
    # if i<1:
    start1 = time.time()
    popt, pcov = curve_fit(dynamic, xdata, combination, method='trf',
                    bounds=([1, 1, 0.1, 0.1], [10, 10, 1, 1]), xtol=0.001)
    end1 = time.time()
    print("Param: ", popt)
    m1, m2, l1, l2 = popt

    combination_fit = dynamic(xdata, m1, m2, l1, l2)

    #split combined arrays
    th1_fit, th2_fit = np.degrees(np.array_split(combination_fit, 2))
    th1_mess, th2_mess = np.degrees(np.array_split(combination, 2))
    th1_buf = th1_mess - th1_fit
    th1_dif = np.append(th1_dif,th1_buf[:,None],axis=1)
    xdata, xdata2  = np.array_split(xdata, 2)

    #timesteps for prior
    dataframes = np.arange(its)
    X = np.atleast_2d([dataframes]).T
    #timesteps for posterior
    dataframes = np.arange(its)
    X_prior = np.atleast_2d([dataframes]).T
    #predictive steps
    x = np.atleast_2d(np.linspace(1,its,(its)*10)).T

    ##combination of individual set noises
    print("1", th1_dif[:,-meanover:])
    th1_dif_togp = np.mean(th1_dif[:,-meanover:], axis=1)
    print("2", th1_dif_togp)

    #gaussian process model
    kernel = MAT(length_scale=5, nu=3/2) + WHT(noise_level=1)
    gp = GaussianProcessRegressor(kernel=kernel, n_restarts_optimizer=3)
    start2 = time.time()
    gp.fit(X,th1_dif_togp)
    y_pred_1, sigma_1 = gp.predict(x, return_std=True)
    end2 = time.time()

    #add gp to fit and scale
    th1_fitgp_mu = th1_fit + y_pred_1[::10]
    #scale down for visualisation
    th1_fitgp_sigma = sigma_1[::10]
    #residual noise after fitting and gaussian process
    th1_fitgp_noise = th1_mess - th1_fitgp_mu
    th1_fitgp_noise_mse  = np.mean((th1_mess - th1_fitgp_mu)**2)
    th1_fitgp_noise_rmse  = np.sqrt(np.mean((th1_mess - th1_fitgp_mu)**2))
    th1_fit_noise = th1_mess - th1_fit
    th1_fit_noise_mse = np.mean((th1_mess - th1_fit)**2)
    th1_fit_noise_rmse = np.sqrt(np.mean((th1_mess - th1_fit)**2))


    #storage for analysis
    mse_noise_dp = np.append(mse_noise_dp, th1_fit_noise_mse)
    mse_noise_gp = np.append(mse_noise_gp, th1_fitgp_noise_mse)
    rmse_noise_dp = np.append(rmse_noise_dp, th1_fit_noise_rmse)
    rmse_noise_gp = np.append(rmse_noise_gp, th1_fitgp_noise_rmse)

    #time meaurements
    time_dp = start1 - end1
    time_gp = start2 - end2
    mse_time_dp = np.append(mse_time_dp, time_dp)
    mse_time_gp = np.append(mse_time_gp, time_gp)

    '''############################ END: Program Run ###############################'''
    '''#############################################################################'''
    '''############################ START: Visualisation ###########################'''

    #plot 1
    line1, = ax1.plot(xdata, th1_fit, 'b-',
                        label=r'$\theta_{1,gefittet}$')
    line2, = ax1.plot(xdata, th1_mess, 'ro',
                        markersize=1, label=r'$\theta_{1,mess}$')
    ax1.legend()
    #plot 2
    line3, = ax2.plot(xdata, th1_fit, 'b-',
                        markersize=1, label=r'$\theta_{1 gefittet}$')
    line4, = ax2.plot(xdata, th1_dif[:,-1:], 'ro',
                        markersize=2, label=r'$\theta_{1,mess - \theta_1,fit}$')
    line5, = ax2.plot(x*0.01*steps, y_pred_1, 'k-',
                        markersize=1, label=r'$\theta_{1,GP}$')
    col1 = ax2.fill_between(x[:,0]*0.01*steps, y_pred_1 - 1.96*sigma_1,y_pred_1 + 1.96*sigma_1,
                            alpha=0.2, color='k', label=u'95 % Vertrauensintervall')
    ax2.legend()
    #plot 3
    line6, = ax3.plot(xdata, th1_fitgp_mu, 'g-',
                        markersize=1, label=r'$\theta_{1,DP+GP}$')
    line7, = ax3.plot(xdata, th1_fit, 'b-',
                        markersize=1, label=r'$\theta_{1,fitted}$')
    line8, = ax3.plot(xdata, th1_mess, 'ro',
                        markersize=2, label=r'$\theta_{1,mess}$')
    col2 = ax3.fill_between(xdata, th1_fitgp_mu - 1.96*th1_fitgp_sigma,th1_fitgp_mu + 1.96*th1_fitgp_sigma,
                        alpha=0.2, color='k', label=u'95 % Vertrauensintervall')
    ax3.legend()
    #plot 4
    line9, = ax4.plot(xdata, th1_fitgp_noise, 'bo', markersize=1, label=r'$\theta_{1,Rest Modell+GP}$')
    line10, = ax4.plot(xdata, th1_fit_noise, 'ro', markersize=1, label=r'$\theta_{1,Rest Modell}$')
    ax4.legend()

    fig.canvas.draw()
    fig.canvas.flush_events()
    ax1.lines.remove(line1)
    ax1.lines.remove(line2)
    ax2.lines.remove(line3)
    ax2.lines.remove(line4)
    ax2.lines.remove(line5)
    ax2.collections.remove(col1)
    ax3.lines.remove(line6)
    ax3.lines.remove(line7)
    ax3.lines.remove(line8)
    ax3.collections.remove(col2)
    ax4.lines.remove(line9)
    ax4.lines.remove(line10)

    #initial file value
    if i<1:
        #print configs to file
        print("Versuch:".ljust(40) + "%s" % (file),
            file=open("Protokolle/%s.txt" % file, "a"))
        print("Messrauschen:".ljust(40) + "%s" % (rauschen),
            file=open("Protokolle/%s.txt" % file, "a"))
        print("DP-Identifikation:".ljust(40) + "%s" % (dp_anzahl),
            file=open("Protokolle/%s.txt" % file, "a"))
        print("GP-Kernel:".ljust(40) +"%s" % kernel,
            file=open("Protokolle/%s.txt" % file, "a"))
        print("Trainingsdaten:".ljust(40) +"%i" % training,
            file=open("Protokolle/%s.txt" % file, "a"))
        print("Schrittweite:".ljust(40) + "%i" % steps,
            file=open("Protokolle/%s.txt" % file, "a"))
        print("Mittelwertweite:".ljust(40) + "%i" % meanover,
            file=open("Protokolle/%s.txt" % file, "a"))
        print("",
            file=open("Protokolle/%s.txt" % file, "a"))

    #for pipeline simulation
    time.sleep(5)
    # print("Time Modell:".ljust(40) + "%f" % time_dp, file=open("Protokolle/%s.txt" % file, "a"))
    # print("Time Modell + GP:".ljust(40) + "%f" % time_gp, file=open("Protokolle/%s.txt" % file, "a"))
    # print("MSE Modell:".ljust(40) + "%f" % th1_fit_noise_mse, file=open("Protokolle/%s.txt" % file, "a"))
    # print("MSE Modell:".ljust(40) + "%f" % np.abs(th1_fit_noise_mse).max(), file=open("Protokolle/%s.txt" % file, "a"))
    # print("MSE Modell + GP:".ljust(40) + "%f" % th1_fitgp_noise_mse.round(3), file=open("Protokolle/%s.txt" % file, "a"))
    # print("", file=open("Protokolle/%s.txt" % file, "a"))
    i = i+1

print("MSE Modell:",
        file=open("Protokolle/%s.txt" % file, "a"))
print(mse_noise_dp,
        file=open("Protokolle/%s.txt" % file, "a"))
print("",
        file=open("Protokolle/%s.txt" % file, "a"))
print("MSE Modell + GP:",
        file=open("Protokolle/%s.txt" % file, "a"))
print(mse_noise_gp,
        file=open("Protokolle/%s.txt" % file, "a"))
print("RMSE Modell:",
        file=open("Protokolle/%s.txt" % file, "a"))
print(rmse_noise_dp,
        file=open("Protokolle/%s.txt" % file, "a"))
print("",
        file=open("Protokolle/%s.txt" % file, "a"))
print("RMSE Modell + GP:",
        file=open("Protokolle/%s.txt" % file, "a"))
print(rmse_noise_gp,
        file=open("Protokolle/%s.txt" % file, "a"))


print("",
        file=open("Protokolle/%s.txt" % file, "a"))
print("Aufzeichnung Mittelwerte",
        ile=open("Protokolle/%s.txt" % file, "a"))
print("",
        file=open("Protokolle/%s.txt" % file, "a"))
print("Mittelwert Time Modell:".ljust(40) + "%f" % np.mean(mse_time_dp),
        file=open("Protokolle/%s.txt" % file, "a"))
print("Mittelwert Time GP:".ljust(40) + "%f" % np.mean(mse_time_gp),
        file=open("Protokolle/%s.txt" % file, "a"))
print("Maximum MSE Modell:".ljust(40) + "%f" % np.abs(mse_noise_dp).max(),
        file=open("Protokolle/%s.txt" % file, "a"))
print("Maximum MSE Modell + GP:".ljust(40) + "%f" % np.abs(mse_noise_gp).max(),
        file=open("Protokolle/%s.txt" % file, "a"))
print("Mittelwert MSE Modell:".ljust(40) + "%f" % np.mean(mse_noise_dp),
        file=open("Protokolle/%s.txt" % file, "a"))
print("Mittelwert MSE Modell + GP:".ljust(40) + "%f" % np.mean(mse_noise_gp),
        file=open("Protokolle/%s.txt" % file, "a"))
print("Maximum RMSE Modell:".ljust(40) + "%f" % np.abs(rmse_noise_dp).max(),
        file=open("Protokolle/%s.txt" % file, "a"))
print("Maximum RMSE Modell + GP:".ljust(40) + "%f" % np.abs(rmse_noise_gp).max(),
        file=open("Protokolle/%s.txt" % file, "a"))
print("Mittelwert RMSE Modell:".ljust(40) + "%f" % np.mean(rmse_noise_dp),
        file=open("Protokolle/%s.txt" % file, "a"))
print("Mittelwert RMSE Modell + GP:".ljust(40) + "%f" % np.mean(rmse_noise_gp),
        file=open("Protokolle/%s.txt" % file, "a"))

'''############################ END: Visualisation #############################'''
'''#############################################################################'''
