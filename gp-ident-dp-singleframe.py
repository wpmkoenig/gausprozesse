'''#############################################################################'''
'''Autor: Luca König'''
'''Datum: Februar 2020'''
'''Programm: Modellidentifikation mit Gauss Prozess und DP-Identifikation       '''
'''#############################################################################'''
'''############################ START: Imports #################################'''
#Visualisation
import matplotlib.pyplot as plt
#Maths and Calculation
import math
import numpy as np
from numpy.linalg import inv
from scipy.stats import norm
#Input Matlab File
import scipy.io as sio
#Curve Fitting Library
from scipy.optimize import curve_fit
#Gaussian Process
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, ConstantKernel as C, RationalQuadratic as RQ, WhiteKernel as WHT, ExpSineSquared as EXP, Matern as MAT
from sklearn import preprocessing
#time
import time
#import MODUL for Double Pendulum Dynamics
import module_dp as dp

#Dataset of Measurement Values
mat_ang_hip = sio.loadmat('C:/Users/Luca/Documents/03_Studium/6.Semester/Bachelorarbeit/Code/Messdaten/ascent_hip_angles.mat') #Einlesen der .mat-Datei
mat_ang_knee = sio.loadmat('C:/Users/Luca/Documents/03_Studium/6.Semester/Bachelorarbeit/Code/Messdaten/ascent_knee_angles.mat') #Einlesen der .mat-Datei
data_ang_hip = mat_ang_hip['hip']
data_ang_knee = mat_ang_knee['knee']
data_ang_hip = data_ang_hip.T
data_ang_knee = data_ang_knee.T

'''############################ END: Imports ###################################'''
'''#############################################################################'''
'''############################ START: Parameter ###############################'''

### for real measurement data
#width of trainingset per frame
training = 150
#choice of frames
sets = 100
#steps of trainingdata
steps = 1
#Theta 1 and Theta 2 values
x1 = data_ang_hip[:training:steps,sets]  #Theta1
x1 = x1.reshape(-1)
x2 = data_ang_knee[:training:steps,sets]  #Theta2
x2 = x2.reshape(-1)
#converting to degree
x1 = x1*180/np.pi
x2 = x2*180/np.pi
#for curve fitting
combination = np.concatenate((x1,x2))

### for dp identification
#gravitational acceleration
g = 9.8

### scaling parameters
#time distance between samples
T = 0.010
#Recordtime
recordtime = training*T
#scaling axis for combinated data
xdata = np.linspace(0,recordtime*2,training*2)

'''############################ END: Parameter #################################'''
'''#############################################################################'''
'''############################ START: Functions ###############################'''

def dynamic(xdata, m1, m2, l1, l2):
    #empty inital arrays
    # m1 = m1*10
    # m2 = m2*10
    th1 = np.array([0.01,0])
    th2 = np.array([0.01,0])
    for k in range(0,training):
        #Simplificated parameters
        a1 = (m1+m2)*l1**2
        a2 = m2*l1*l2
        a3 = (m1+m2)*l1
        a4 = m2*l2**2
        a5 = m2*l2

        #Dynamics Simplification
        A = a1*a4-(a2*np.cos(th1[k]-th2[k]))**2
        s1 = -a2*((th2[k+1]-th2[k])/T)**2*np.sin(th1[k]-th2[k])-g*a3*np.sin(th1[k])
        s2 = a2*((th1[k+1]-th1[k])/T)**2*np.sin(th1[k]-th2[k])-g*a5*np.sin(th2[k])

        #Pendulum Dynamic
        th1 = np.append(th1, (2*th1[k+1]-th1[k]+T**2*(a4*s1-s2*a2*np.cos(th1[k]-th2[k]))/A))
        th2 = np.append(th2, (2*th2[k+1]-th2[k]+T**2*(-s1*a2*np.cos(th1[k]-th2[k])+a1*s2)/A))
    combination_fit = np.concatenate((th1[:-2],th2[:-2]))
    return combination_fit

'''############################ END: Functions #################################'''
'''#############################################################################'''
'''############################ START: Gaussian Distribution ###################'''

popt, pcov = curve_fit(dynamic, xdata, combination, method='trf', bounds=([3, 2, 0.2, 0.1], [10, 8, 0.8, 0.5]), xtol=0.001)
print(popt)
m1, m2, l1, l2 = popt
combination_fit = dynamic(xdata, m1, m2, l1, l2)

#split combined arrays
th1_fit, th2_fit = np.array_split(combination_fit, 2)
th1_mess, th2_mess = np.array_split(combination, 2)
th1_dif = th1_fit - th1_mess
th2_dif = th2_fit - th2_mess
xdata, xdata2  = np.array_split(xdata, 2)

#Anzahl der Zeitschritte für Regression
dataframes = np.arange(training)
X = np.atleast_2d([dataframes]).T
#Anzahl der Zeitschritte für Vorhersage
dataframes = np.arange(training)
X_prior = np.atleast_2d([dataframes]).T
#Vorhersage Zeitschritte
x = np.atleast_2d(np.linspace(1,training,(training)*10)).T

#Gauss Prozess Modell instanziieren
kernel = MAT(length_scale=5, nu=3/2) + WHT(noise_level=1)
gp = GaussianProcessRegressor(kernel=kernel, n_restarts_optimizer=3)
gp.fit(X,th1_dif)
y_pred_1, sigma_1 = gp.predict(x, return_std=True)

#add gp to fit and scale
th1_fitgp_mu = th1_fit + np.absolute(y_pred_1[::10])
#scale down for visualisation
th1_fitgp_sigma = sigma_1[::10]

#residual noise after fitting and gaussian process
th1_fitgp_noise = th1_mess - th1_fitgp_mu

'''############################ END: Program Run ###############################'''
'''#############################################################################'''
'''############################ START: Visualisation ###########################'''

plt.subplot(221)
plt.plot(xdata, th1_fit, 'b-', label="Th1 Gefittet")
plt.plot(xdata, th1_mess, 'ro', markersize=1, label=u'Th1 Messung')
plt.xlabel('Timesteps in s')
plt.ylabel('Angle in rad')
plt.title('Theta 1 Gefittet')
plt.legend()

plt.subplot(222)
plt.plot(xdata, th1_fit, 'b-', markersize=1, label=u'Th1 Gefittet')
plt.plot(xdata, th1_dif, 'ro', markersize=2, label=u'Th1 Mess - Fit')
plt.plot(x*0.01, y_pred_1, 'k-', markersize=1, label=u'Th1 Gauss Process')
plt.fill_between(x[:,0]*0.01, y_pred_1 - 1.96*sigma_1,y_pred_1 + 1.96*sigma_1, alpha=0.2, color='k', label=u'95 % confidence interval')
plt.xlabel('Timesteps in s')
plt.ylabel('Angle in rad')
plt.title('Theta 1 GP über Messwerte - Fit')
plt.legend()

plt.subplot(223)
plt.plot(xdata, th1_fitgp_mu, 'b-', markersize=1, label=u'Th1 Model + Gauss')
plt.plot(xdata, th1_fit, 'g-', markersize=1, label=u'Th1 Model + Gauss')
plt.plot(xdata, th1_mess, 'ro', markersize=2, label=u'Th1 Gemessen')
plt.fill_between(xdata, th1_fitgp_mu - 1.96*th1_fitgp_sigma,th1_fitgp_mu + 1.96*th1_fitgp_sigma, alpha=0.2, color='k', label=u'95 % confidence interval')
plt.xlabel('Timesteps in s')
plt.ylabel('Angle in rad')
plt.title('Theta 1 Fit + GP')
plt.legend()

plt.subplot(224)
plt.plot(xdata, th1_fitgp_noise, 'b-', label="Th2 Gefittet")
plt.xlabel('Timesteps in s')
plt.ylabel('Angle in rad')
plt.title('Theta 1 Residual Noise after Fit + GP')
plt.legend()
plt.show()


'''############################ END: Visualisation #############################'''
'''#############################################################################'''
