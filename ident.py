'''#############################################################################
Autor:
 Luca König
Datum:
 August 2020
Beschreibung:
 Systemidentifikation anhand von Messdaten

################################################################################'''
'''############################ START: Imports #################################'''

import numpy as np
import scipy.io as sio
from matplotlib import pyplot as plt
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, ConstantKernel as C, RationalQuadratic as RQ, WhiteKernel as WHT, ExpSineSquared as Exp, Matern as MAT
from sklearn import preprocessing

#Einlesen der .mat-Datei
mat_ang_hip = sio.loadmat('C:/Users/Luca/Documents/03_Studium/Hilfskraft-Job/Code/exoskelett/DP_Reconstruct/Python/ascent_hip_angles.mat')
mat_ang_knee = sio.loadmat('C:/Users/Luca/Documents/03_Studium/Hilfskraft-Job/Code/exoskelett/DP_Reconstruct/Python/ascent_knee_angles.mat')
#Filtern des Datenarrays
data_ang_hip = mat_ang_hip['hip']
data_ang_knee = mat_ang_knee['knee']

'''############################ END: Imports ###################################'''
'''#############################################################################'''
'''############################ START: Data Troubleshooting ####################'''

'''############################ END: Data Troubleshooting ######################'''
'''#############################################################################'''
'''############################ START: Parameter ###############################'''

#Schneide Spalten vorn und hinten am Array ab
source_data_slice_start = 0
source_data_slice_end = 300
data_ang_hip = data_ang_hip[:,source_data_slice_start:source_data_slice_end]
data_ang_knee = data_ang_knee[:,source_data_slice_start:source_data_slice_end]

# data_ang_hip = np.reshape(data_ang_hip,(1, -1))
# data_ang_knee = np.reshape(data_ang_knee,(1, -1))

#Wählt einen Datensatz an
multiplicant = 0
datarange = source_data_slice_end-source_data_slice_start
startslice = datarange*multiplicant
spanwidth = 1
endslice = startslice+datarange*spanwidth

'''############################ END: Parameter  ################################'''
'''#############################################################################'''
'''############################ START: Gaussian Regressian #####################'''

#Wählt Datenstreifen und konvertiert zur relativen Winkeln in Grad
x1 = (data_ang_hip[:,startslice:endslice])*180/np.pi  #Theta1
x2 = (((data_ang_knee[:,startslice:endslice])*180/np.pi)+x1) #Theta2

# x1_scaled = preprocessing.scale(x1)
# x2_scaled = preprocessing.scale(x2)
x1_scaled = x1
x2_scaled = x2

y = np.asarray([x1_scaled, x2_scaled]).T
nsamples, nx, ny = y.shape
d2_y = y.reshape((nsamples,nx*ny))
print("Form Trainingsset:",d2_y.shape)
print("Dimensionen Trainingsset:",d2_y.ndim)

print("Anzahl Daten:",len(y))

#Anzahl der Zeitschritte
dataframes = np.arange(300.0*spanwidth)
X = np.atleast_2d([dataframes]).T
print("Zeitschritte:", len(X))

#Interpolationsschritte
x = np.atleast_2d(np.linspace(1,300*spanwidth,30000*spanwidth)).T
print("Interpolationsschritte", len(x))

#Gauss Prozess Modell instanziieren
kernel = MAT(length_scale=1, nu=3/2) + WHT(noise_level=1)
gp = GaussianProcessRegressor(kernel=kernel, n_restarts_optimizer=4)

gp.fit(X,d2_y)
y_pred_1, sigma_1 = gp.predict(x, return_std=True)

plt.plot(X, d2_y[:,:], 'ro', markersize=1, alpha=0.2, label=u'Observation')
plt.plot(x, y_pred_1[:,0], 'b-', linewidth=1, label=u'Prediction')
plt.fill_between(x[:,0], y_pred_1[:,0] - 1.96*sigma_1,y_pred_1[:,0] + 1.96*sigma_1, alpha=0.2, color='k', label=u'95 % confidence interval')
plt.xlabel('Timesteps')
# plt.legend(loc='upper right', fontsize=10)
plt.ylim(-3,3)
plt.show()
