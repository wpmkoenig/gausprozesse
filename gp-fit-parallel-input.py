'''#############################################################################'''
'''Autor: Luca König'''
'''Datum: September 2020'''
'''Programm: Gauss Prozess über mehrere Datensequenzen                          '''
'''#############################################################################'''
'''############################ START: Imports #################################'''

import sys
import matplotlib.pyplot as plt
import math
import numpy as np
from numpy.linalg import inv
import scipy.io as sio
from scipy.optimize import curve_fit
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, ConstantKernel as C, RationalQuadratic as RQ, WhiteKernel as WHT, ExpSineSquared as EXP, Matern as MAT
from sklearn import preprocessing
# np.set_printoptions(threshold=sys.maxsize)

mat_data = sio.loadmat('C:/Users/Luca/Documents/03_Studium/Hilfskraft-Job/Code/exoskelett/DP_Reconstruct/Matlab/Data2_noisy.mat') #Einlesen der .mat-Datei
data_mess = mat_data['Save_Data2_noisy']    #Filtern des Datenarrays

'''############################ END: Imports ###################################'''
'''#############################################################################'''
'''############################ START: Parameter ###############################'''

#gravitational acceleration
g = 9.8

#trainig and validation
training = 300
steps = 1

#measurement data synthetic
# x1 = data_mess[:training:steps,0]  #Theta1
# x2 = data_mess[:training:steps,1]  #Theta2
# T1 = data_mess[:training,2]  #M1
# T2 = data_mess[:training,3]  #M2
# combination = np.concatenate((x1,x2))

#measurement recorded
mat_ang_hip = sio.loadmat('C:/Users/Luca/Documents/03_Studium/6.Semester/Bachelorarbeit/Code/Messdaten/ascent_hip_angles.mat') #Einlesen der .mat-Datei
mat_ang_knee = sio.loadmat('C:/Users/Luca/Documents/03_Studium/6.Semester/Bachelorarbeit/Code/Messdaten/ascent_knee_angles.mat') #Einlesen der .mat-Datei
mat_torq_hip = sio.loadmat('C:/Users/Luca/Documents/03_Studium/6.Semester/Bachelorarbeit/Code/Messdaten/ascent_hip_moment.mat') #Einlesen der .mat-Datei
mat_torq_knee = sio.loadmat('C:/Users/Luca/Documents/03_Studium/6.Semester/Bachelorarbeit/Code/Messdaten/ascent_knee_moment.mat') #Einlesen der .mat-Datei

data_ang_hip = mat_ang_hip['hip']    #Filtern des Datenarrays
data_ang_knee = mat_ang_knee['knee']    #Filtern des Datenarrays
data_torq_hip = mat_torq_hip['hipmom']    #Filtern des Datenarrays
data_torq_knee = mat_torq_knee['kneemom']    #Filtern des Datenarrays

data_ang_hip = data_ang_hip.T
data_ang_knee = data_ang_knee.T

#choose part of array for data selection
x1 = data_ang_hip[:training:steps,:10]  #Theta1
x2 = data_ang_knee[:training:steps,:10]  #Theta2
print(x1)
T1 = data_torq_hip[:training]  #M1
T2 = data_torq_knee[:training]  #M2

#converting to degree
x1 = x1*180/np.pi
x2 = x2*180/np.pi
combination = np.concatenate((x1,x2))

#for sampling time 15s
T = 0.010 #incremental time step
recordtime = training*T #in ss
timesteps = training/steps #amount of time steps while recordtime
posteriorsteps = int(timesteps*0)
xdata = np.linspace(0,recordtime*2,timesteps*2)

'''############################ END: Parameter #################################'''
'''#############################################################################'''
'''############################ START: Functions ###############################'''

'''############################ END: Functions #################################'''
'''#############################################################################'''
'''############################ START: Gaussian Process ########################'''

#split combined arrays
th1_mess, th2_mess = np.array_split(combination, 2)
xdata, xdata2  = np.array_split(xdata, 2)

#Anzahl der Zeitschritte für Regression
dataframes = np.arange(timesteps)
X = np.tile(np.atleast_2d([dataframes]).T,(1,len(x1.T)))
print(X)

#Anzahl der Zeitschritte für Vorhersage
dataframes = np.arange(timesteps+posteriorsteps)
X_prior = np.atleast_2d([dataframes]).T

#Vorhersage Zeitschritte
x = np.atleast_2d(np.linspace(1,timesteps+posteriorsteps,(timesteps+posteriorsteps)*10)).T
x = np.tile(x,(1,len(x1.T)))

#Gauss Prozess Modell instanziieren
kernel = MAT(length_scale=10, nu=5/2) + WHT(noise_level=1)
gp = GaussianProcessRegressor(kernel=kernel, n_restarts_optimizer=3)
gp.fit(X,x1)
y_pred_1, sigma_1 = gp.predict(x, return_std=True)

'''############################ END: Gaussian Process ##########################'''
'''#############################################################################'''
'''############################ START: Debugging ###############################'''

print("np.shape(x1):",np.shape(x1))
print("np.shape(X):", np.shape(X))
print("np.shape(x):", np.shape(x))
print("steps:", posteriorsteps)
print("np.shape(x):",np.shape(x))

'''############################ END: Debugging #################################'''
'''#############################################################################'''
'''############################ START: Visualisation ###########################'''

plt.subplot(111)
plt.plot(X, x1, 'ro', markersize=2, alpha=1, label=u'Observation')
plt.plot(x[:,0], y_pred_1[:,0], 'b-', linewidth=2, label=u'Prediction')
plt.fill_between(x[:,0], y_pred_1[:,0] - 1.96*sigma_1, y_pred_1[:,0] + 1.96*sigma_1, alpha=0.2, color='k', label=u'95 % confidence interval')
plt.xlabel('Timesteps')
plt.show()
