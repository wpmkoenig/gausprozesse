'''#############################################################################'''
'''Autor: Luca König'''
'''Datum: September 2020'''
'''Programm: Gaussian Process over Distributions                                '''
'''#############################################################################'''
'''############################ START: Imports #################################'''

#Visualisation
import matplotlib.pyplot as plt
#Maths and Calculation
import math
import numpy as np
from numpy.linalg import inv
from scipy.stats import norm
#Input Matlab File
import scipy.io as sio
#Gaussian Process
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, ConstantKernel as C, RationalQuadratic as RQ, WhiteKernel as WHT, ExpSineSquared as EXP, Matern as MAT
from sklearn import preprocessing
#time
import time

#Load Dataset from .mat-File
mat_data = sio.loadmat('C:/Users/Luca/Documents/03_Studium/Hilfskraft-Job/Code/exoskelett/DP_Reconstruct/Matlab/Data2_noisy.mat') #Einlesen der .mat-Datei
data_mess = mat_data['Save_Data2_noisy']    #Filtern des Datenarrays

'''############################ END: Imports ###################################'''
'''#############################################################################'''
'''############################ START: Parameter ###############################'''

#gravitational acceleration
g = 9.8

#trainig and validation
training = 300
sets = 10
steps = 1

# measurement data synthetic
x1 = data_mess[:training:steps,0]  #Theta1
x2 = data_mess[:training:steps,1]  #Theta2
T1 = data_mess[:training,2]  #M1
T2 = data_mess[:training,3]  #M2
combination = np.concatenate((x1,x2))

#measurement recorded
mat_ang_hip = sio.loadmat('C:/Users/Luca/Documents/03_Studium/6.Semester/Bachelorarbeit/Code/Messdaten/ascent_hip_angles.mat') #Einlesen der .mat-Datei
mat_ang_knee = sio.loadmat('C:/Users/Luca/Documents/03_Studium/6.Semester/Bachelorarbeit/Code/Messdaten/ascent_knee_angles.mat') #Einlesen der .mat-Datei
mat_torq_hip = sio.loadmat('C:/Users/Luca/Documents/03_Studium/6.Semester/Bachelorarbeit/Code/Messdaten/ascent_hip_moment.mat') #Einlesen der .mat-Datei
mat_torq_knee = sio.loadmat('C:/Users/Luca/Documents/03_Studium/6.Semester/Bachelorarbeit/Code/Messdaten/ascent_knee_moment.mat') #Einlesen der .mat-Datei

data_ang_hip = mat_ang_hip['hip']
data_ang_knee = mat_ang_knee['knee']
data_torq_hip = mat_torq_hip['hipmom']
data_torq_knee = mat_torq_knee['kneemom']

data_ang_hip = data_ang_hip.T
data_ang_knee = data_ang_knee.T

#choose part of array for data selection
x1 = data_ang_hip[:training:steps,:sets]  #Theta1
x2 = data_ang_knee[:training:steps,:sets]  #Theta2
T1 = data_torq_hip[:training]  #M1
T2 = data_torq_knee[:training]  #M2

#converting to degree
x1 = x1*180/np.pi
x2 = x2*180/np.pi
combination = np.concatenate((x1,x2))

#for sampling time 15s
T = 0.010 #incremental time step
recordtime = training*T #in ss
timesteps = training/steps #amount of time steps while recordtime
posteriorsteps = int(timesteps*0)
xdata = np.linspace(0,recordtime*2,timesteps*2)

#for gaussian distribution
std = []
mu = []

#visualisation
custom = 100

'''############################ END: Parameter #################################'''
'''#############################################################################'''
'''############################ START: Debugging ###############################'''

print("x1:", x1)

'''############################ END: Debugging #################################'''
'''#############################################################################'''
'''############################ START: Functions ###############################'''

'''############################ END: Functions #################################'''
'''#############################################################################'''
'''############################ START: Gaussian Distribution ###################'''

#add synthetic noise to data
noise = np.random.normal(0,0.5,x1.shape)
x1 = x1+noise

for i in range(len(x1)):
    std= np.append(std, np.std(x1[i,:]))
    mu = np.append(mu, np.mean(x1[i,:]))

scale_x = np.linspace(-1.5, 2, 100)
p = norm.pdf(scale_x,mu[custom],std[custom])

#Anzahl der Zeitschritte für Regression
tmp = np.arange(training)
X = np.atleast_2d(tmp).T

#Anzahl der Zeitschritte für Vorhersage
tmp = np.arange(training*10)
X_prior = np.atleast_2d([tmp])

#Vorhersage Zeitschritte
x = np.atleast_2d(np.linspace(1,training,training*10)).T

kernel = MAT(length_scale=10, nu=3/2) + WHT(noise_level=1)
gp = GaussianProcessRegressor(kernel=kernel, n_restarts_optimizer=3)
gp.fit(X,mu)
y_pred_1, sigma_1 = gp.predict(x, return_std=True)

print(np.shape(x))
print(np.shape(y_pred_1))
'''############################ END: Gaussian Distribution #####################'''
'''#############################################################################'''
'''############################ START: Visualisation ###########################'''

plt.subplot(211)
plt.hist(x1[custom,:], bins=124, density=True, alpha=0.6, color='b')
plt.plot(scale_x, p, 'k-', linewidth=2)
plt.xlabel('Timesteps')
plt.subplot(212)
plt.plot(X, mu, 'ro', markersize=2, alpha=1, label=u'Observation')
plt.plot(x, y_pred_1, 'b-', alpha=0.6,linewidth=2, label=u'Prediction')
plt.fill_between(x[:,0], y_pred_1 - 1.96*sigma_1, y_pred_1 + 1.96*sigma_1, alpha=0.2, color='k', label=u'95 % confidence interval')
plt.xlabel('Timesteps')
plt.show()
