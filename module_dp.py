'''#############################################################################'''
'''Autor: Luca König'''
'''Datum: September 2020'''
'''Programm: Dynamik des Doppelpendels                                          '''
'''#############################################################################'''
'''############################ MODUL: Double Pendulum Dynamics ################'''

#Maths and Calculation
import math
import numpy as np

def dynamic(xdata, m1, m2, l1, l2, T):
    #empty inital arrays
    # m1 = m1*10
    # m2 = m2*10
    th1 = np.array([0.01,0])
    th2 = np.array([0.01,0])
    for k in range(0,300):
        #Simplificated parameters
        a1 = (m1+m2)*l1**2
        a2 = m2*l1*l2
        a3 = (m1+m2)*l1
        a4 = m2*l2**2
        a5 = m2*l2

        #Dynamics Simplification
        A = a1*a4-(a2*np.cos(th1[k]-th2[k]))**2
        s1 = -a2*((th2[k+1]-th2[k])/T)**2*np.sin(th1[k]-th2[k])-g*a3*np.sin(th1[k])
        s2 = a2*((th1[k+1]-th1[k])/T)**2*np.sin(th1[k]-th2[k])-g*a5*np.sin(th2[k])

        #Pendulum Dynamic
        th1 = np.append(th1, (2*th1[k+1]-th1[k]+T**2*(a4*s1-s2*a2*np.cos(th1[k]-th2[k]))/A))
        th2 = np.append(th2, (2*th2[k+1]-th2[k]+T**2*(-s1*a2*np.cos(th1[k]-th2[k])+a1*s2)/A))
    combination_fit = np.concatenate((th1[:-2],th2[:-2]))
    return combination_fit
